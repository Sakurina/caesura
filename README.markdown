# Caesura

*Caesura* is a jukebox-style music player app I plan to write for Apple platforms (iOS/macOS) meant to recapture the magic of iTunes 4-era music players on modern devices/operating systems.

## Rationale

*iTunes* has always been a polarizing piece of software. You either really hated it, or it was so resonant with how you personally wanted to manage your music that nothing else really ever came close to competing with it. I adored *iTunes*, and I think the 4.x version cycle may be the best music player software ever made.

As mainstream music consumption shifted towards streaming services over the last decade, *iTunes* would become more and more hostile to users preferring to use an offline, hand-curated music library, in favor of pushing people towards *Apple Music* subscriptions.

*Caesura* is not trying to reinvent music listening. It's just trying to offer a modern-day take on *iTunes 4*-era music player software across all of Apple's platforms. It will probably be just as polarizing as *iTunes* originally was, but that's exactly what I want, and that's what I'm going to make.

## A Trivial Technology

*Caesura* is an experiment in adhering to the principles of [trivial technologies][tt]. In practical terms, this means:

* **Trivial:** The project should be developed in a straight-forward way that is easily understandable and accessible to new and old developers alike.
* **Malleable:** The project should be structured in an easily extensible way, such that combined with the accessibility, users should feel empowered to add any features they want or need themselves.
* **Open:** Do whatever you please with this project's source code, as it is licensed under a public domain-equivalent license (0BSD).

[tt]: https://trivial.technology

