// SPDX-License-Identifier: 0BSD

import Cocoa

@main
class AppDelegate: NSObject, NSApplicationDelegate, MenuStateHandler {
    
    // MARK: Properties
    // - outlets
    @IBOutlet var removeFromLibraryItem : NSMenuItem?
    @IBOutlet var removeFromPlaylistItem : NSMenuItem?
    @IBOutlet var renamePlaylistItem : NSMenuItem?
    @IBOutlet var deletePlaylistItem : NSMenuItem?
    @IBOutlet var repeatOffItem : NSMenuItem?
    @IBOutlet var repeatAllItem : NSMenuItem?
    @IBOutlet var repeatOneItem : NSMenuItem?
    @IBOutlet var shuffleOffItem : NSMenuItem?
    @IBOutlet var shuffleOnItem : NSMenuItem?
    
    // - instance variables
    public var coordinator : MacAppCoordinator?
    
    // MARK: App delegate implementation
    
    func applicationDidFinishLaunching(_ notification: Notification) {
        self.coordinator = MacAppCoordinator(menuStateHandler: self)
        coordinator?.start()
    }
    
    // MARK: Menu bar actions
    
    @IBAction func showMainWindow(sender: Any?) {
        self.coordinator?.navigateToMainWindow()
    }
    
    @IBAction func importTrackToLibrary(sender: Any) {
        self.coordinator?.importTrackToLibrary()
    }
    
    @IBAction func openPrefsWindow(sender: Any) {
        self.coordinator?.navigateToPreferences()
    }
    
    @IBAction func playPauseMenuItemSelected(sender: Any?) {
        self.coordinator?.togglePlayback()
    }
    
    @IBAction func previousTrackMenuItemSelected(sender: Any?) {
        self.coordinator?.moveToPreviousTrack()
    }
    
    @IBAction func nextTrackMenuItemSelected(sender: Any?) {
        self.coordinator?.moveToNextTrack()
    }
    
    // MARK: Menu bar state management

    func setRepeatModeMenuItemState(_ repeatMode: RepeatMode) {
        repeatOffItem?.state = (repeatMode == .off) ? .on : .off
        repeatAllItem?.state = (repeatMode == .all) ? .on : .off
        repeatOneItem?.state = (repeatMode == .one) ? .on : .off
    }
    
    func setShuffleModeMenuItemState(_ shuffleEnabled: Bool) {
        shuffleOffItem?.state = (!shuffleEnabled) ? .on : .off
        shuffleOnItem?.state = (shuffleEnabled) ? .on : .off
    }
    
    @IBAction func repeatMenuItemSelected(sender: Any?) {
        guard let unwrapped = sender else { return }
        guard let menuItem = unwrapped as? NSMenuItem else { return }

        if menuItem === repeatOffItem {
            self.coordinator?.repeatMode = .off
        } else if menuItem === repeatAllItem {
            self.coordinator?.repeatMode = .all
        } else if menuItem === repeatOneItem {
            self.coordinator?.repeatMode = .one
        }
    }
    
    @IBAction func shuffleMenuItemSelected(sender: Any?) {
        guard let unwrapped = sender else { return }
        guard let menuItem = unwrapped as? NSMenuItem else { return }

        if menuItem === shuffleOffItem {
            self.coordinator?.shuffleEnabled = false
        } else if menuItem === shuffleOnItem {
            self.coordinator?.shuffleEnabled = true
        }
    }
        
    func enableSelectionMenuItems(selected: Bool, isLibrary: Bool) {
        if (isLibrary) {
            // main library collection
            self.removeFromLibraryItem?.isEnabled = selected
            self.removeFromPlaylistItem?.isEnabled = false
            self.deletePlaylistItem?.isEnabled = false
            self.renamePlaylistItem?.isEnabled = false
        } else {
            // playlist, etc...
            self.removeFromLibraryItem?.isEnabled = false
            self.removeFromPlaylistItem?.isEnabled = selected
            self.deletePlaylistItem?.isEnabled = true
            self.renamePlaylistItem?.isEnabled = true
        }
    }
}

