// SPDX-License-Identifier: 0BSD

import Foundation
import AVFoundation

struct MetadataFields {
    var title : String?
    var artist : String?
    var album : String?
    var genre : String?
    var grouping : String?
    var year : String?
    var track_num : Int?
    var disc_num : Int?
}

class MetadataExtractor {
    
    static func extract(_ fileUrl: URL) -> MetadataFields {
        
        let player = AVPlayer(url: fileUrl)
        var result = MetadataFields()

        let metadata = player.currentItem?.asset.commonMetadata ?? []
        for d in metadata {
            if d.commonKey == .commonKeyTitle {
                result.title = d.stringValue
            }
            if d.commonKey == .commonKeyArtist {
                result.artist = d.stringValue
            }
            if d.commonKey == .commonKeyAlbumName {
                result.album = d.stringValue
            }
            if d.commonKey == .quickTimeMetadataKeyGenre {
                result.genre = d.stringValue
            }
            if d.commonKey == .iTunesMetadataKeyGrouping {
                result.grouping = d.stringValue
            }
            if d.commonKey == .id3MetadataKeyYear {
                result.year = d.stringValue
            }
            if d.commonKey == .id3MetadataKeyTrackNumber {
                result.track_num = d.numberValue?.intValue
            }
            if result.track_num == nil && d.commonKey == .iTunesMetadataKeyTrackNumber {
                result.track_num = d.numberValue?.intValue
            }
            if d.commonKey == .iTunesMetadataKeyDiscNumber {
                result.disc_num = d.numberValue?.intValue
            }
        }

        return result
    }
    
}
