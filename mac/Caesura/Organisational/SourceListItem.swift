//
//  SourceListItem.swift
//  Caesura
//
//  Created by Yanik on 2021-10-04.
//

import Foundation

class SourceListItem : NSObject {
    
    init(collectionType: CollectionType, title: String?) {
        self.collectionType = collectionType
        self.title = title ?? "untitled playlist"
    }
    
    var collectionType : CollectionType
    var title : String
    
    static func == (lhs: SourceListItem, rhs: SourceListItem) -> Bool {
        return lhs.collectionType == rhs.collectionType
    }

}
