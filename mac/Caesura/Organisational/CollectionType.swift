// SPDX-License-Identifier: 0BSD

import Foundation

enum CollectionType : Equatable {
    
    // MARK: Collection types
    
    case library
    case orderedPlaylist(id: Int)
    
    // MARK: Equatable protocol implementation
    
    public static func ==(lhs: CollectionType, rhs: CollectionType) -> Bool {
        switch (lhs, rhs) {
            case (.library, .library):
                return true
            case (.orderedPlaylist(let a), .orderedPlaylist(let b)):
                return a == b
            default:
                return false
        }
    }
}
