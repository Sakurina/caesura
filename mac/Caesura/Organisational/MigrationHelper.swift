// SPDX-License-Identifier: 0BSD

import Foundation
import FMDB

class MigrationHelper {
    
    var databasePath: String
    
    static func appSupportDirectoryPath() -> String? {
        let appSupportSearchPaths = NSSearchPathForDirectoriesInDomains(.applicationSupportDirectory, .userDomainMask, true)
        if appSupportSearchPaths.count == 0 {
            return nil
        }
        let rootDirectoryPath = appSupportSearchPaths[0] as NSString
        let appDirectoryPath = rootDirectoryPath.appendingPathComponent("Caesura")
        return appDirectoryPath
    }
    
    static func defaultLibraryPath() -> String? {
        guard let appSupportDir = appSupportDirectoryPath() else {
            return nil
        }
        do {
            try FileManager.default.createDirectory(atPath: appSupportDir,
                                            withIntermediateDirectories: true,
                                            attributes: nil)
            let nsAppSupportDir = appSupportDir as NSString
            let dbPath = nsAppSupportDir.appendingPathComponent("Caesura Library.caelib")
            return dbPath
        } catch {
            return nil
        }
    }
    
    static func currentLibraryPath() -> String? {
        var libraryPath = UserDefaults.standard.string(forKey: "LibraryPath")
        if libraryPath != nil && !FileManager.default.fileExists(atPath: libraryPath!) {
            UserDefaults.standard.removeObject(forKey: "LibraryPath")
            libraryPath = defaultLibraryPath()
        } else if libraryPath == nil {
            libraryPath = defaultLibraryPath()
        }
        return libraryPath
    }
    
    required init(databasePath: String) {
        self.databasePath = databasePath
    }
    
    func runMigrations() -> Void {
        let db = FMDatabase(path: self.databasePath)
        let couldOpen = db.open()
        if !couldOpen {
            return
        }
        
        var version = 0
        
        if (!db.tableExists("caesura_schema")) {
            // initialize database with a schema version table if one is not found
            db.executeStatements("CREATE TABLE caesura_schema(version INT)")
            do {
                try db.executeUpdate("INSERT INTO caesura_schema VALUES(0)", values: nil)
            } catch {
                
            }
        } else {
            // look up current schema version from existing schema version table
            do {
                let versionQuery = try db.executeQuery("SELECT version FROM caesura_schema", values: nil)
                while versionQuery.next() {
                    version = Int(versionQuery.int(forColumn: "version"))
                }
            } catch {
                
            }
        }
        
        // whenever a database schema change needs to take place,
        // create a new migration function and conditionally run it like this
        if (version < 1) {
            migration001(db)
            version = 1
        }
        if (version < 2) {
            migration002(db)
            version = 2
        }

        db.close()
    }
    
    func migration001(_ db: FMDatabase) -> Void {
        db.executeStatements("""
                            CREATE TABLE track (
                                id INTEGER PRIMARY KEY AUTOINCREMENT,
                                title TEXT,
                                artist TEXT,
                                album TEXT,
                                genre TEXT,
                                grouping TEXT,
                                year TEXT,
                                track_num INTEGER(8),
                                disc_num INTEGER(8),
                                play_count INTEGER(8),
                                skip_count INTEGER(8),
                                last_played TEXT,
                                date_added TEXT,
                                file_url TEXT);
                            """)
        db.executeStatements("""
                             CREATE TABLE ordered_playlist (
                               id INTEGER PRIMARY KEY AUTOINCREMENT,
                               title TEXT,
                               sort_mode TEXT);
                             """)
        db.executeStatements("""
                             CREATE TABLE ordered_playlist_track (
                               id INTEGER PRIMARY KEY AUTOINCREMENT,
                               track_id INTEGER(8),
                               ordered_playlist_id INTEGER(8),
                               user_order TEXT);
                             """)
        do {
            try db.executeUpdate("UPDATE caesura_schema SET version = 1", values: nil)
        } catch {
            
        }
    }
    
    func migration002(_ db: FMDatabase) -> Void {
        // transform ordered_playlist_track's user_order column from a text column into an integer column
        // 1. rename old column
        db.executeStatements("ALTER TABLE ordered_playlist_track RENAME COLUMN user_order TO user_order_old")
        // 2. create new column
        db.executeStatements("ALTER TABLE ordered_playlist_track ADD COLUMN user_order INTEGER(8)")
        // 3. copy values over
        do {
            try db.executeUpdate("UPDATE ordered_playlist_track SET user_order = user_order_old WHERE user_order_old IS NOT NULL", values: nil)
        } catch {
            
        }
        // 4. delete old column
        db.executeStatements("ALTER TABLE ordered_playlist_track DROP COLUMN user_order_old")
        
        do {
            try db.executeUpdate("UPDATE caesura_schema SET version = 2", values: nil)
        } catch {
            
        }
    }
}
