// SPDX-License-Identifier: 0BSD

import Foundation
import AppKit

class Track : NSObject, NSPasteboardWriting {

    init(id: Int, title: String?, artist: String?, album: String?, genre: String?, grouping: String?, year: String?, track_num: Int?, disc_num: Int?, play_count: Int?, skip_count: Int?, last_played: Date?, date_added: Date?, file_url: String?, order: Int, ordered_playlist_track_id: Int?) {
        self.id = id
        self.title = title
        self.artist = artist
        self.album = album
        self.genre = genre
        self.grouping = grouping
        self.year = year
        self.track_num = track_num
        self.disc_num = disc_num
        self.play_count = play_count
        self.skip_count = skip_count
        self.last_played = last_played
        self.date_added = date_added
        self.file_url = file_url
        self.order = order
        self.ordered_playlist_track_id = ordered_playlist_track_id
    }
    
    var id : Int;
    var title : String?
    var artist : String?
    var album : String?
    var genre : String?
    var grouping : String?
    var year : String?
    var track_num : Int?
    var disc_num : Int?
    var play_count : Int?
    var skip_count : Int?
    var last_played : Date?
    var date_added : Date?
    var file_url : String?
    var order : Int;
    var ordered_playlist_track_id : Int?
    
    // MARK: Drag & drop support
    
    func writableTypes(for pasteboard: NSPasteboard) -> [NSPasteboard.PasteboardType] {
        var res = [NSPasteboard.PasteboardType("net.r-ch.caesura.track")]
        if ordered_playlist_track_id != nil {
            res.append(NSPasteboard.PasteboardType("net.r-ch.caesura.track-within-playlist"))
        }
        return res
    }
    
    func pasteboardPropertyList(forType type: NSPasteboard.PasteboardType) -> Any? {
        if type.rawValue == "net.r-ch.caesura.track" {
            return id
        } else if type.rawValue == "net.r-ch.caesura.track-within-playlist" {
            return ordered_playlist_track_id!
        } else {
            return nil
        }
    }
    
    func contextualID(_ collectionType: CollectionType) -> Int? {
        switch collectionType {
            case .orderedPlaylist(_): return ordered_playlist_track_id
            default: return id
        }
    }

}
