// SPDX-License-Identifier: 0BSD

import Foundation
import FMDB

class LibraryService {
    
    var database : FMDatabase
    
    required init(_ dbPath: String) {
        self.database = FMDatabase(path: dbPath)
        self.database.open()
    }
    
    // COMMANDS
    // Updates and writes to the database
    
    func addToLibraryCommand(trackFileURL: URL, metadata: MetadataFields) {
        do {
            let now = Date()
            try database.executeUpdate("""
                INSERT INTO track (title, artist, album, genre, grouping, year, track_num, disc_num, play_count, skip_count, last_played, date_added, file_url)
                VALUES(?, ?, ?, ?, ?, ?, ?, ?, 0, 0, null, ?, ?)
            """, values: [
                metadata.title as Any,
                metadata.artist as Any,
                metadata.album as Any,
                metadata.genre as Any,
                metadata.grouping as Any,
                metadata.year as Any,
                metadata.track_num as Any,
                metadata.disc_num as Any,
                now as Any,
                trackFileURL as Any
            ])
        } catch {
            
        }
    }
    
    func removeFromLibraryCommand(trackID: Int) {
        do {
            try database.executeUpdate("DELETE FROM ordered_playlist_track WHERE track_id = ?", values: [trackID])
            try database.executeUpdate("DELETE FROM track WHERE id = ?", values: [trackID])
        } catch {
            
        }
    }
    
    func createOrderedPlaylistCommand(name: String) {
        do {
            try database.executeUpdate("INSERT INTO ordered_playlist (title, sort_mode) VALUES(?, 'order')", values: [name])
        } catch {
            
        }
    }
    
    func deleteOrderedPlaylistCommand(orderedPlaylistID: Int) {
        do {
            try database.executeUpdate("DELETE FROM ordered_playlist_track WHERE ordered_playlist_id = ?", values: [orderedPlaylistID])
            try database.executeUpdate("DELETE FROM ordered_playlist WHERE id = ?", values: [orderedPlaylistID])
        } catch {

        }
    }
    
    func renamePlaylistCommand(orderedPlaylistID: Int, newName: String) {
        do {
            try database.executeUpdate("UPDATE ordered_playlist SET title = ? WHERE id = ?", values: [newName, orderedPlaylistID])
        } catch {
            
        }
    }
    
    func addTrackToOrderedPlaylistCommand(trackID: Int, orderedPlaylistID: Int) {
        do {
            try database.executeUpdate("INSERT INTO ordered_playlist_track (track_id, ordered_playlist_id, user_order) VALUES(?, ?, ?)", values: [trackID, orderedPlaylistID, Int.max])
        } catch {
            
        }
        recalculateOrderColumnForOrderedPlaylistCommand(orderedPlaylistID: orderedPlaylistID)
    }
    
    func removeTrackFromOrderedPlaylistCommand(orderedPlaylistTrackID: Int, orderedPlaylistID: Int) {
        do {
            try database.executeUpdate("DELETE FROM ordered_playlist_track WHERE id = ?", values: [orderedPlaylistTrackID])
        } catch {
            
        }
        recalculateOrderColumnForOrderedPlaylistCommand(orderedPlaylistID: orderedPlaylistID)
    }
    
    func recalculateOrderColumnForOrderedPlaylistCommand(orderedPlaylistID: Int) {
        var dict = Dictionary<Int, Int>()
        let sql = "SELECT * FROM ordered_playlist_track WHERE ordered_playlist_id = ? ORDER BY user_order"
        do {
            let q = try database.executeQuery(sql, values: [orderedPlaylistID])
            var i = 1
            while q.next() {
                let id = Int(q.int(forColumn: "id"))
                dict[id] = i
                i = i + 1
            }
            for (i, o) in dict {
                try database.executeUpdate("UPDATE ordered_playlist_track SET user_order = ? WHERE id = ?", values: [o, i])
            }
        } catch {
            
        }

    }
    
    func reorderInOrderedPlaylistCommand(orderedPlaylistTrackID: Int, orderedPlaylistID: Int, targetOrderIndex: Int) {
        var dict = Dictionary<Int, Int>()
        let sql = "SELECT * FROM ordered_playlist_track WHERE ordered_playlist_id = ? AND id <> ? ORDER BY user_order"
        do {
            let q = try database.executeQuery(sql, values: [orderedPlaylistID, orderedPlaylistTrackID])
            var i = 1
            dict[orderedPlaylistTrackID] = targetOrderIndex + 1
            while q.next() {
                if i == (targetOrderIndex + 1) {
                    i = i + 1
                }
                let id = Int(q.int(forColumn: "id"))
                dict[id] = i
                i = i + 1
            }
            
            if (targetOrderIndex + 1) > dict.count {
                dict[orderedPlaylistTrackID] = dict.count
            }
            
            for (i, o) in dict {
                try database.executeUpdate("UPDATE ordered_playlist_track SET user_order = ? WHERE id = ?", values: [o, i])
            }
        } catch {
            
        }

    }
    
    func saveOrderedPlaylistSortMode(orderedPlaylistID: Int, sortMode: String) {
        do {
            try database.executeUpdate("UPDATE ordered_playlist SET sort_mode = ? WHERE id = ?", values: [sortMode, orderedPlaylistID])
        } catch {
            
        }
    }
    
    func increasePlayCountForTrack(trackID: Int) {
        do {
            let now = Date()
            try database.executeUpdate("UPDATE track SET play_count = play_count + 1, last_played = ? WHERE id = ?", values: [now, trackID])
        } catch {
            
        }
    }
    
    func increaseSkipCountForTrack(trackID: Int) {
        do {
            try database.executeUpdate("UPDATE track SET skip_count = skip_count + 1 WHERE id = ?", values: [trackID])
        } catch {
            
        }
    }
    
    // QUERIES
    // Looking up information in the database
    
    func libraryCollectionQuery(keywordFilter: String?, sortMode: String?, ascending: Bool) -> [Track] {
        var res = [Track]()
        do {
            let sqlPrefix = "SELECT * FROM track"
            var sqlWhereSuffix = ""
            var sqlOrderSuffix = ""
            
            if (sortMode == nil || sortMode == "CollectionOrder") {
                // order
                if ascending {
                    sqlOrderSuffix = " ORDER BY id ASC"
                } else {
                    sqlOrderSuffix = " ORDER BY id DESC"
                }
            } else if (sortMode == "CollectionTitle") {
                // title
                if ascending {
                    sqlOrderSuffix = " ORDER BY title COLLATE NOCASE ASC"
                } else {
                    sqlOrderSuffix = " ORDER BY title COLLATE NOCASE DESC"
                }
            } else if (sortMode == "CollectionArtist") {
                // artist
                if ascending {
                    sqlOrderSuffix = " ORDER BY artist COLLATE NOCASE ASC"
                } else {
                    sqlOrderSuffix = " ORDER BY artist COLLATE NOCASE DESC"
                }
            } else if (sortMode == "CollectionAlbum") {
                // album
                if ascending {
                    sqlOrderSuffix = " ORDER BY album COLLATE NOCASE ASC, disc_num ASC, track_num ASC"
                } else {
                    sqlOrderSuffix = " ORDER BY album COLLATE NOCASE DESC, disc_num ASC, track_num ASC"
                }
            } else if (sortMode == "CollectionSkipCount") {
                if ascending {
                    sqlOrderSuffix = " ORDER BY skip_count ASC, id ASC"
                } else {
                    sqlOrderSuffix = " ORDER BY skip_count DESC, id ASC"
                }
            } else if (sortMode == "CollectionPlayCount") {
                if ascending {
                    sqlOrderSuffix = " ORDER BY play_count ASC, id ASC"
                } else {
                    sqlOrderSuffix = " ORDER BY play_count DESC, id ASC"
                }
            } else if (sortMode == "CollectionLastPlayed") {
                if ascending {
                    sqlOrderSuffix = " ORDER BY last_played ASC, id ASC"
                } else {
                    sqlOrderSuffix = " ORDER BY last_played DESC, id ASC"
                }
            }
            
            var values : [Any] = []
            if let keyword = keywordFilter {
                let likeKeyword = "%" + keyword + "%"
                sqlWhereSuffix = " WHERE title LIKE ? OR artist LIKE ? OR album LIKE ?"
                values.append(likeKeyword)
                values.append(likeKeyword)
                values.append(likeKeyword)
            }
            
            let sql = sqlPrefix + sqlWhereSuffix + sqlOrderSuffix
            let q = try database.executeQuery(sql, values: values)
            while q.next() {
                let t = Track(id: Int(q.int(forColumn: "id")),
                              title: q.string(forColumn: "title"),
                              artist: q.string(forColumn: "artist"),
                              album: q.string(forColumn: "album"),
                              genre: q.string(forColumn: "genre"),
                              grouping: q.string(forColumn: "grouping"),
                              year: q.string(forColumn: "year"),
                              track_num: Int(q.int(forColumn: "track_num")),
                              disc_num: Int(q.int(forColumn: "disc_num")),
                              play_count: Int(q.int(forColumn: "play_count")),
                              skip_count: Int(q.int(forColumn: "skip_count")),
                              last_played: q.date(forColumn: "last_played"),
                              date_added: q.date(forColumn: "date_added"),
                              file_url: q.string(forColumn: "file_url"),
                              order: Int(q.int(forColumn: "id")),
                              ordered_playlist_track_id: nil)
                res.append(t)
            }
        } catch {
            
        }
        return res
    }
    
    func orderedPlaylistCollectionQuery(orderedPlaylistID: Int, keywordFilter: String?, sortMode: String?, ascending: Bool) -> [Track] {
        var res = [Track]()
        do {
            let sqlPrefix = "SELECT t.id, t.title, t.artist, t.album, t.genre, t.grouping, t.year, t.track_num, t.disc_num, t.play_count, t.skip_count, t.last_played, t.date_added, t.file_url, opt.user_order AS [order], opt.id AS [ordered_playlist_track_id] FROM track t JOIN ordered_playlist_track opt ON t.id = opt.track_id"
            var sqlWhereSuffix = " WHERE opt.ordered_playlist_id = ?"
            var sqlOrderSuffix = ""
            var values : [Any] = []
            values.append(orderedPlaylistID)
            
            
            if (sortMode == nil || sortMode == "CollectionOrder") {
                // order
                if ascending {
                    sqlOrderSuffix = " ORDER BY opt.user_order ASC"
                } else {
                    sqlOrderSuffix = " ORDER BY opt.user_order DESC"
                }
            } else if (sortMode == "CollectionTitle") {
                // title
                if ascending {
                    sqlOrderSuffix = " ORDER BY t.title COLLATE NOCASE ASC"
                } else {
                    sqlOrderSuffix = " ORDER BY t.title COLLATE NOCASE DESC"
                }
            } else if (sortMode == "CollectionArtist") {
                // artist
                if ascending {
                    sqlOrderSuffix = " ORDER BY t.artist COLLATE NOCASE ASC"
                } else {
                    sqlOrderSuffix = " ORDER BY t.artist COLLATE NOCASE DESC"
                }
            } else if (sortMode == "CollectionAlbum") {
                // album
                if ascending {
                    sqlOrderSuffix = " ORDER BY t.album COLLATE NOCASE ASC, t.disc_num ASC, t.track_num ASC"
                } else {
                    sqlOrderSuffix = " ORDER BY t.album COLLATE NOCASE DESC, t.disc_num ASC, t.track_num ASC"
                }
            } else if (sortMode == "CollectionSkipCount") {
                if ascending {
                    sqlOrderSuffix = " ORDER BY t.skipCount ASC, opt.user_order ASC"
                } else {
                    sqlOrderSuffix = " ORDER BY t.skipCount DESC, opt.user_order ASC"
                }
            } else if (sortMode == "CollectionPlayCount") {
                if ascending {
                    sqlOrderSuffix = " ORDER BY t.playCount ASC, opt.user_order ASC"
                } else {
                    sqlOrderSuffix = " ORDER BY t.playCount DESC, opt.user_order ASC"
                }
            } else if (sortMode == "CollectionLastPlayed") {
                if ascending {
                    sqlOrderSuffix = " ORDER BY t.lastPlayed ASC, opt.user_order ASC"
                } else {
                    sqlOrderSuffix = " ORDER BY t.lastPlayed DESC, opt.user_order ASC"
                }
            }
            
            if let keyword = keywordFilter {
                let likeKeyword = "%" + keyword + "%"
                sqlWhereSuffix = sqlWhereSuffix + " AND (t.title LIKE ? OR t.artist LIKE ? OR t.album LIKE ?)"
                values.append(likeKeyword)
                values.append(likeKeyword)
                values.append(likeKeyword)
            }
            
            let sql = sqlPrefix + sqlWhereSuffix + sqlOrderSuffix
            let q = try database.executeQuery(sql, values: values)
            while q.next() {
                let t = Track(id: Int(q.int(forColumn: "id")),
                              title: q.string(forColumn: "title"),
                              artist: q.string(forColumn: "artist"),
                              album: q.string(forColumn: "album"),
                              genre: q.string(forColumn: "genre"),
                              grouping: q.string(forColumn: "grouping"),
                              year: q.string(forColumn: "year"),
                              track_num: Int(q.int(forColumn: "track_num")),
                              disc_num: Int(q.int(forColumn: "disc_num")),
                              play_count: Int(q.int(forColumn: "play_count")),
                              skip_count: Int(q.int(forColumn: "skip_count")),
                              last_played: q.date(forColumn: "last_played"),
                              date_added: q.date(forColumn: "date_added"),
                              file_url: q.string(forColumn: "file_url"),
                              order: Int(q.int(forColumn: "order")),
                              ordered_playlist_track_id: Int(q.int(forColumn: "ordered_playlist_track_id")))
                res.append(t)
            }
        } catch {
            
        }
        return res

    }
    
    func sourceListItemsQuery() -> [SourceListItem] {
        var res = [SourceListItem]()
        
        // fixed library node
        res.append(SourceListItem(collectionType: .library, title: "Library"))
        
        // ordered playlists
        let orderedQuery = "SELECT * FROM ordered_playlist ORDER BY LOWER(title)"
        do {
            let q = try database.executeQuery(orderedQuery, values: nil)
            while q.next() {
                let playlistID = Int(q.int(forColumn: "id"))
                let playlistTitle = q.string(forColumn: "title")
                let sli = SourceListItem(collectionType: .orderedPlaylist(id: playlistID), title: playlistTitle)
                res.append(sli)
            }
        } catch {
            
        }
        return res
    }
}
