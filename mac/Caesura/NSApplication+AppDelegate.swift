import Foundation
import AppKit

extension NSApplication {
    
    func castedAppDelegate() -> AppDelegate {
        return delegate as! AppDelegate
    }
    
}
