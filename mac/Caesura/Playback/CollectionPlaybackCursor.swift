// SPDX-License-Identifier: 0BSD

import Foundation

enum RepeatMode: Int {
    case off = 0
    case all = 1
    case one = 2
}

class CollectionPlaybackCursor {
    
    // MARK: Initializer(s)
    
    init(tracks: [Track], startPlaybackFrom: Track?, collectionType: CollectionType) {
        self.tracks = tracks
        self.currentTrack = startPlaybackFrom
        self.collectionType = collectionType
    }
    
    // MARK: Properties
    
    var tracks : [Track]
    var currentTrack: Track?
    var collectionType : CollectionType
    var repeatMode : RepeatMode = .off
    var shuffleEnabled : Bool = false
    var shufflePreviousTrackHistory : [Int] = []
    
    // MARK: Previous/next track accessors
    
    func previousTrack() -> Track? {
        guard let current = currentTrack else {
            return nil
        }
        
        guard let currentTrackIndex = tracks.firstIndex(of: current) else {
            return nil
        }
        let previousTrackIndex = tracks.index(before: currentTrackIndex)
        
        if repeatMode == .one {
            return current
        }
        
        if shuffleEnabled {
            let previousTrackID = shufflePreviousTrackHistory.last
            if previousTrackID == nil {
                return nil
            }
            let previousShuffleTrackIndex = tracks.map({ $0.contextualID(self.collectionType) }).firstIndex(of: previousTrackID)
            if previousShuffleTrackIndex == nil {
                return nil
            }
            return tracks[previousShuffleTrackIndex!]
        } else {
            if previousTrackIndex >= 0 {
                return tracks[previousTrackIndex]
            } else if repeatMode == .all {
                return tracks.last
            }
        }
        
        return nil
    }
    
    func nextTrack() -> Track? {
        guard let current = currentTrack else {
            return nil
        }
        
        guard let currentTrackIndex = tracks.firstIndex(of: current) else {
            return nil
        }
        let nextTrackIndex = tracks.index(after: currentTrackIndex)
        
        if repeatMode == .one {
            return current
        }
        
        if shuffleEnabled {
            // get tracks whose IDs were not already played
            // tracks excluding those whose IDs are in the previous shuffle tracks IDs array
            let unplayedShuffleTracks = tracks.filter({ !shufflePreviousTrackHistory.contains($0.contextualID(self.collectionType) ?? -1) })
            if unplayedShuffleTracks.isEmpty {
                return nil
            }
            // randomly select one
            let randomlySelectedIndex = Int.random(in: 0..<unplayedShuffleTracks.count)
            // return it
            return unplayedShuffleTracks[randomlySelectedIndex]
        } else {
            if nextTrackIndex < tracks.count {
                return tracks[nextTrackIndex]
            } else if repeatMode == .all {
                return tracks.first
            }
        }
        
        return nil
    }
    
    // MARK: Previous/next track actions
    
    func moveToPreviousTrack() {
        self.currentTrack = previousTrack()
        if shuffleEnabled && self.repeatMode != .one {
            // remove last item in the array of previous shuffle IDs
            if !shufflePreviousTrackHistory.isEmpty {
                shufflePreviousTrackHistory.removeLast()
            }
        }
    }
    
    func moveToNextTrack() {
        if shuffleEnabled && self.repeatMode != .one {
            // add current track ID to previous shuffle IDs
            if self.currentTrack != nil {
                let trackContextualID = self.currentTrack!.contextualID(self.collectionType) ?? -1
                shufflePreviousTrackHistory.append(trackContextualID)
            }
            // if we are repeating and there are no unplayed tracks for this loop of the playlist,
            // empty the previously played shuffle track IDs array
            if self.repeatMode == .all {
                let unplayedShuffleTracks = tracks.filter({ !shufflePreviousTrackHistory.contains($0.contextualID(self.collectionType) ?? -1) })
                if unplayedShuffleTracks.isEmpty {
                    shufflePreviousTrackHistory = []
                }
            }
        }
        self.currentTrack = nextTrack()
    }
    
    // MARK: Update tracks in response to collection contents changes
    
    func updateTracks(newTracks: [Track]) {
        let postChangeTrackIds = newTracks.map({ $0.contextualID(self.collectionType) })

        if currentTrack != nil {
            // find current track in new tracks
            let index = postChangeTrackIds.firstIndex(of: self.currentTrack!.contextualID(self.collectionType))
            if index != nil {
                self.currentTrack = newTracks[index!]
            }
        }
        
        if shuffleEnabled {
            // clear shuffle previous track history of IDs that no longer exist within the collection
            let postChangeShuffleHistory = shufflePreviousTrackHistory.filter({ postChangeTrackIds.contains($0) })
            self.shufflePreviousTrackHistory = postChangeShuffleHistory
        }
        
        self.tracks = newTracks
    }
}
