// SPDX-License-Identifier: 0BSD

import Foundation
import AVFoundation

class CollectionPlayer {
    
    // MARK: Initializer(s)
    
    init(playbackCursor: CollectionPlaybackCursor) {
        self.audioPlayer = AVPlayer()
        self.playbackCursor = playbackCursor
    }
    
    // MARK: Properties
    
    var audioPlayer : AVPlayer
    var playbackCursor : CollectionPlaybackCursor
    
    // MARK: Playback/transport actions
    
    func isPlaying() -> Bool {
        return audioPlayer.rate > 0
    }
    
    func play() {
        if audioPlayer.currentItem == nil {
            if playbackCursor.currentTrack != nil {
                let item = playerItem(track: playbackCursor.currentTrack!)
                NotificationCenter.default.addObserver(self,
                                                       selector: #selector(playerItemDidPlayToEndTime),
                                                       name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
                                                       object: item)
                NotificationCenter.default.post(name: CaesuraNotification.trackChangedNotification, object: playbackCursor.currentTrack)
                audioPlayer.replaceCurrentItem(with: item)
            }
        }
        audioPlayer.play()
        NotificationCenter.default.post(name: CaesuraNotification.playerStartedPlayingNotification, object: nil)
    }
    
    func pause() {
        audioPlayer.pause()
        NotificationCenter.default.post(name: CaesuraNotification.playerStoppedPlayingNotification, object: nil)

    }
        
    func previousTrack() {
        NotificationCenter.default.removeObserver(self)
        playbackCursor.moveToPreviousTrack()
        let newCurrent = playbackCursor.currentTrack
        if newCurrent == nil {
            // no new track, update interface to clearly indicate playback has stopped
            self.audioPlayer.replaceCurrentItem(with: nil)
            NotificationCenter.default.post(name: CaesuraNotification.playerStoppedPlayingNotification, object: nil)

        } else {
            let item = playerItem(track: newCurrent!)
            NotificationCenter.default.addObserver(self,
                                                   selector: #selector(playerItemDidPlayToEndTime),
                                                   name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
                                                   object: item)
            NotificationCenter.default.post(name: CaesuraNotification.trackChangedNotification, object: newCurrent)
            audioPlayer.replaceCurrentItem(with: item)
        }
    }
    
    func nextTrack() {
        NotificationCenter.default.removeObserver(self)
        playbackCursor.moveToNextTrack()
        
        let newCurrent = playbackCursor.currentTrack
        if newCurrent == nil {
            // no new track, update interface to clearly indicate playback has stopped
            self.audioPlayer.replaceCurrentItem(with: nil)
            NotificationCenter.default.post(name: CaesuraNotification.playerStoppedPlayingNotification, object: nil)
        } else {
            let item = playerItem(track: newCurrent!)
            NotificationCenter.default.addObserver(self,
                                                   selector: #selector(playerItemDidPlayToEndTime),
                                                   name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
                                                   object: item)
            NotificationCenter.default.post(name: CaesuraNotification.trackChangedNotification, object: newCurrent)
            audioPlayer.replaceCurrentItem(with: item)
        }
    }
    
    
    // MARK: Interfacing tracks to media items
    
    func playerItem(track: Track) -> AVPlayerItem? {
        guard let urlString = track.file_url else {
            return nil
        }
        
        guard let url = URL(string: urlString) else {
            return nil
        }
        
        return AVPlayerItem(url: url)
    }
    
    // MARK: Handling player item completion
    
    @objc func playerItemDidPlayToEndTime(notification: Notification) {
        let endedTrack = playbackCursor.currentTrack!;
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.post(name: CaesuraNotification.trackPlayedAccountingNotification, object: endedTrack)
        playbackCursor.moveToNextTrack()
        
        let newCurrent = playbackCursor.currentTrack
        if newCurrent == nil {
            // no new track, update interface to clearly indicate playback has stopped
            self.audioPlayer.replaceCurrentItem(with: nil)
            NotificationCenter.default.post(name: CaesuraNotification.playerStoppedPlayingNotification, object: nil)
        } else {
            // there is a new track, keep playing
            let item = playerItem(track: newCurrent!)
            NotificationCenter.default.addObserver(self,
                                                   selector: #selector(playerItemDidPlayToEndTime),
                                                   name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
                                                   object: item)
            
            NotificationCenter.default.post(name: CaesuraNotification.trackChangedNotification, object: newCurrent!)
            audioPlayer.replaceCurrentItem(with: item)
            audioPlayer.play()
        }
    }
}
