// SPDX-License-Identifier: 0BSD

import Foundation

class CaesuraNotification {
    // MARK: Custom notification names
    // The only notifications left for now are going to be playback related
    
    static let trackChangedNotification = NSNotification.Name(rawValue: "CaesuraTrackChanged")
    static let playerStartedPlayingNotification = NSNotification.Name(rawValue: "CaesuraPlayerStartedPlaying")
    static let playerStoppedPlayingNotification = NSNotification.Name(rawValue: "CaesuraPlayerStoppedPlaying")
    static let trackPlayedAccountingNotification = NSNotification.Name(rawValue: "CaesuraTrackPlayedAccounting")
}
