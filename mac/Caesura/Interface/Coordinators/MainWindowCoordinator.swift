// SPDX-License-Identifier: 0BSD

import Foundation
import AppKit

class MainWindowCoordinator {
    
    // MARK: Initializers
    required init(parentCoordinator: MainWindowSpawner & PlayerController, libraryService: LibraryService) {
        self.parentCoordinator = parentCoordinator
        self.libraryService = libraryService
    }
    
    // MARK: Properties
    // - parent/child coordinators
    var parentCoordinator : MainWindowSpawner & PlayerController
    
    // - window/view controllers
    var mainWindowController : MainWindowController?
    var sourceListReloadable : Reloadable?
    var collectionReloadable : Reloadable?
    
    // - dependencies
    var libraryService : LibraryService
    
    // - coordinator state
    var collectionType : CollectionType = .library
    var sourceListItems : [SourceListItem] = []
    var tracks : [Track] = []
    var sortMode : String = "CollectionOrder"
    var keywordFilter : String? = nil
    var ascending : Bool = true

    // MARK: Flow management
    func start() {
        // initialize & show window
        let storyboard = NSStoryboard.main
        let windowControllerID = NSStoryboard.SceneIdentifier("main-window-controller")
        self.mainWindowController = storyboard?.instantiateController(withIdentifier: windowControllerID) as? MainWindowController
        self.mainWindowController?.coordinator = self
        self.mainWindowController?.showWindow(self)
        
        // load data
        refreshSourceListItems()
        refreshCurrentCollection()
    }
    
    func end() {
        self.mainWindowController?.close()
        self.parentCoordinator.mainWindowClosed()
    }
    
    func focusWindow() {
        self.mainWindowController?.window?.makeKeyAndOrderFront(self)
    }
    
    // MARK: Passthrough player actions
    
    func initiatePlayback(startPlaybackFrom: Track?) {
        parentCoordinator.initiatePlayback(tracks: self.tracks, startPlaybackFrom: startPlaybackFrom, collectionType: self.collectionType)
    }
    
    func togglePlayback() {
        parentCoordinator.togglePlayback()
    }
    
    func moveToPreviousTrack() {
        parentCoordinator.moveToPreviousTrack()
    }
    
    func moveToNextTrack() {
        parentCoordinator.moveToNextTrack()
    }
    
    func stop() {
        parentCoordinator.stop()
    }
    
    func isPlayingFocusedCollection() -> Bool {
        return parentCoordinator.isPlayingCollectionType(self.collectionType)
    }
    
    func isPlayingTrackInFocusedCollection(_ track: Track) -> Bool {
        return parentCoordinator.isPlaying(track: track, inCollectionType: self.collectionType)
    }
    
    func playbackStatusForTrackInFocusedCollection(_ track: Track) -> TrackPlaybackStatus {
        return parentCoordinator.playbackStatusForTrack(track, inCollectionType: self.collectionType)
    }
    
    // MARK: Handling source focus changes
    
    func sourceChanged(_ sourceListItem: SourceListItem) {
        self.collectionType = sourceListItem.collectionType
        refreshCurrentCollection()
    }
    
    func focusedSelectionIsLibrary() -> Bool {
        return self.collectionType == .library
    }
    
    // MARK: Source list item data source
    
    func sourceListItemCount() -> Int {
        return sourceListItems.count
    }
    
    func sourceListItemAtIndex(_ idx: Int) -> SourceListItem {
        return sourceListItems[idx]
    }

    func refreshSourceListItems() {
        self.sourceListItems = libraryService.sourceListItemsQuery()
        self.sourceListReloadable?.reloadData()
        
        // reset selected index in case name changed to something sorted elsewhere
        let newIndex = self.sourceListItems.firstIndex(where: { sli in
            return sli.collectionType == self.collectionType
        })
        guard let newIndex = newIndex else { return }
        self.sourceListReloadable?.focus(newIndex)
    }
    
    // MARK: Track data source
    
    func trackCount() -> Int {
        return tracks.count
    }
    
    func tracksAtRowIndices(_ indices: IndexSet) -> [Track] {
        return tracks
    }
    func trackAtIndex(_ idx: Int) -> Track {
        return tracks[idx]
    }
    
    func refreshCurrentCollection() {
        // get results from the appropriate query for the collection type
        switch self.collectionType {
            case let .orderedPlaylist(id):
                self.tracks = self.libraryService.orderedPlaylistCollectionQuery(
                    orderedPlaylistID: id,
                    keywordFilter: self.keywordFilter,
                    sortMode: self.sortMode,
                    ascending: self.ascending)
            case .library:
                self.tracks = self.libraryService.libraryCollectionQuery(
                    keywordFilter: self.keywordFilter,
                    sortMode: self.sortMode,
                    ascending: self.ascending)
        }
        
        // notify the player that the playing collection is mutated
        if isPlayingFocusedCollection() {
            self.parentCoordinator.playingCollectionMutated(tracks: self.tracks)
        }
        
        // tell the view controller to reload itself
        self.collectionReloadable?.reloadData()
    }
    
    // MARK: Filters
    
    func keywordFilterTextChanged(_ txt: String) {
        self.keywordFilter = txt
        refreshCurrentCollection()
    }
    
    func sortModeChanged(_ sortMode: String, ascending: Bool) {
        self.sortMode = sortMode
        self.ascending = ascending
        refreshCurrentCollection()
    }
    
    func currentlySortingByOrder() -> Bool {
        return self.sortMode == "CollectionOrder"
    }
    
    // MARK: Playlist manipulation
    
    func createOrderedPlaylist() {
        self.libraryService.createOrderedPlaylistCommand(name: "untitled playlist")
        self.refreshSourceListItems()
    }

    func deleteFocusedCollection() {
        // only handle ordered playlists for now
        switch self.collectionType {
            case let .orderedPlaylist(id):
                // send command
                libraryService.deleteOrderedPlaylistCommand(orderedPlaylistID: id)
            
                // if player corresponds to current playlist, end playback
                if self.isPlayingFocusedCollection() {
                    self.stop()
                }
            
                // reload
                self.refreshSourceListItems()
            default: return
        }
    }
    
    func removeTrackFromFocusedCollection(_ track: Track) {
        switch self.collectionType {
            case let .orderedPlaylist(playlistID): removeSelectedTrackFromOrderedPlaylist(track, playlistID: playlistID)
            case .library: removeSelectedTrackFromLibrary(track)
        }
    }

    func removeSelectedTrackFromLibrary(_ track: Track) {
        // call service
        self.libraryService.removeFromLibraryCommand(trackID: track.id)
        
        // if deleted from library and the track is currently playing, kill playback
        if self.parentCoordinator.isPlayingTrack(track) {
            self.stop()
        }

        self.refreshCurrentCollection()
    }
    
    func removeSelectedTrackFromOrderedPlaylist(_ track: Track, playlistID: Int) {
        guard let playlistRowID = track.ordered_playlist_track_id else {
            return
        }
        
        self.libraryService.removeTrackFromOrderedPlaylistCommand(orderedPlaylistTrackID: playlistRowID, orderedPlaylistID: playlistID)
        
        if self.isPlayingTrackInFocusedCollection(track) {
            self.stop()
        }

        self.refreshCurrentCollection()
    }
    
    func renameCollection(_ collectionType: CollectionType, toNewName newName: String) {
        // only works on ordered playlists for now
        switch collectionType {
            case let .orderedPlaylist(id):
                self.libraryService.renamePlaylistCommand(orderedPlaylistID: id, newName: newName)
                self.refreshSourceListItems()
            default: return
        }
    }
    
    func handleAddToCollectionDropOperation(dropDestination: SourceListItem, info: NSDraggingInfo) -> Bool {
        var success = false
        guard let pasteboardItems = info.draggingPasteboard.pasteboardItems else { return success }
        let trackDragRepresentation = NSPasteboard.PasteboardType("net.r-ch.caesura.track")
        
        switch dropDestination.collectionType {
            case let .orderedPlaylist(orderedPlaylistID):
                for pasteboardItem in pasteboardItems {
                    let val = pasteboardItem.propertyList(forType: trackDragRepresentation)
                    if let trackID = val as? Int {
                        self.libraryService.addTrackToOrderedPlaylistCommand(trackID: trackID, orderedPlaylistID: orderedPlaylistID)
                        success = true
                    }
                }
            default: return false // unallowed operation for unhandled collection types
        }
        
        return success
    }
    
    func handleReorderInCollectionDropOperation(rowIndex: Int, info: NSDraggingInfo) -> Bool {
        var success = false
        
        guard let pasteboardItems = info.draggingPasteboard.pasteboardItems else { return success }
        let playlistMemberDragRepresentation = NSPasteboard.PasteboardType("net.r-ch.caesura.track-within-playlist")
        
        var i = 0
        
        switch self.collectionType {
            case let .orderedPlaylist(orderedPlaylistID):
                // item is the source list item that you are dropping onto
                for pasteboardItem in pasteboardItems {
                    let val = pasteboardItem.propertyList(forType: playlistMemberDragRepresentation)
                    if let asInt = val as? Int {
                        self.libraryService.reorderInOrderedPlaylistCommand(orderedPlaylistTrackID: asInt, orderedPlaylistID: orderedPlaylistID, targetOrderIndex: rowIndex + i)
                        success = true
                        i += 1
                    }
                }
            default: return false // unallowed operation for unhandled collection types
        }

        return success
    }
}
