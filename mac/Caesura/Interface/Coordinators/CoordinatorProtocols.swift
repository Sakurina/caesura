// SPDX-License-Identifier: 0BSD

import Foundation

protocol MenuStateHandler {
    func setRepeatModeMenuItemState(_ repeatMode: RepeatMode)
    func setShuffleModeMenuItemState(_ shuffleEnabled: Bool)
}
protocol PlayerController {
    func initiatePlayback(tracks: [Track], startPlaybackFrom: Track?, collectionType: CollectionType)
    func togglePlayback()
    func play()
    func pause()
    func stop()
    func moveToPreviousTrack()
    func moveToNextTrack()
    func isPlayingTrack(_ track: Track) -> Bool
    func isPlayingCollectionType(_ collectionType: CollectionType) -> Bool
    func isPlaying(track: Track, inCollectionType collectionType: CollectionType) -> Bool
    func playingCollectionMutated(tracks: [Track])
    func playbackStatusForTrack(_ track: Track, inCollectionType collectionType: CollectionType) -> TrackPlaybackStatus
}

protocol MainWindowSpawner {
    func navigateToMainWindow()
    func mainWindowClosed()
}

protocol PreferencesSpawner {
    func navigateToPreferences()
    func preferencesWindowClosed(shouldSoftReset: Bool)
}

protocol Reloadable {
    func reloadData()
    func focus(_ index: Int)
}

protocol PathChangeHandler {
    func libraryPathChanged(_ path: String?)
}
