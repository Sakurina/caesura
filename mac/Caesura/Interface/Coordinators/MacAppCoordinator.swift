// SPDX-License-Identifier: 0BSD

import Foundation
import AppKit
import AVFoundation

// The Mac app coordinator handles the initialization of the application
// and state that is global to the application even when a window is not
// currently open.
//
// This means it handles initializing the data access layer, manages media
// playback, and spawns and provides dependencies to child windows

class MacAppCoordinator : MainWindowSpawner, PreferencesSpawner, PlayerController {
    
    // MARK: Initializers
    required init(menuStateHandler: MenuStateHandler) {
        self.menuStateHandler = menuStateHandler
    }
    
    // MARK: Properties
    // - dependencies
    var libraryService : LibraryService?
    var menuStateHandler : MenuStateHandler
    // - app state
    var collectionPlayer : CollectionPlayer?
    var repeatMode : RepeatMode = .off {
        didSet {
            UserDefaults().setValue(repeatMode.rawValue, forKey: "PlaybackRepeatMode")
            collectionPlayer?.playbackCursor.repeatMode = repeatMode
            self.menuStateHandler.setRepeatModeMenuItemState(repeatMode)
        }
    }
    var shuffleEnabled : Bool = false {
        didSet {
            UserDefaults().setValue(shuffleEnabled, forKey: "PlaybackShuffleEnabled")
            collectionPlayer?.playbackCursor.shuffleEnabled = shuffleEnabled
            self.menuStateHandler.setShuffleModeMenuItemState(shuffleEnabled)
        }
    }
    // - child coordinators
    var mainWindowCoordinator : MainWindowCoordinator?
    var preferencesCoordinator : PreferencesCoordinator?
    
    // MARK: Flow management
    func start() {
        // load client prefs
        repeatMode = RepeatMode(rawValue: UserDefaults().integer(forKey: "PlaybackRepeatMode")) ?? RepeatMode.off
        shuffleEnabled = UserDefaults().bool(forKey: "PlaybackShuffleEnabled")
        
        // determine the library path
        let libraryPath = MigrationHelper.currentLibraryPath()
        guard let libPath = libraryPath else {
            fatalError("Unable to load a music library")
        }

        // set up or migrate database if not already done
        let migrationHelper = MigrationHelper(databasePath: libPath)
        migrationHelper.runMigrations()

        // set up library service
        self.libraryService = LibraryService(libPath)

        // register for notifications to bump play count
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(accountForTrackPlayed),
                                               name: CaesuraNotification.trackPlayedAccountingNotification,
                                               object: nil)

        // open the main window at launch
        navigateToMainWindow()
    }
    
    func end() {
        stop()
        self.libraryService = nil
        if let main = mainWindowCoordinator {
            main.end()
        }
        if let prefs = preferencesCoordinator {
            prefs.end()
        }
    }
    
    func navigateToMainWindow() {
        // Only one instance of the main window should be open at a time
        // Handle this "singleton" by checking if there is an existing coordinator instance declared
        if mainWindowCoordinator == nil {
            self.mainWindowCoordinator = MainWindowCoordinator(parentCoordinator: self, libraryService: self.libraryService!)
            self.mainWindowCoordinator?.start()
        } else {
            self.mainWindowCoordinator?.focusWindow()
        }
    }
    
    func navigateToPreferences() {
        // Only one instance of the preferences window should be open at a time
        // Handle this "singleton" by checking if there is an existing coordinator instance declared
        if preferencesCoordinator == nil {
            self.preferencesCoordinator = PreferencesCoordinator(parentCoordinator: self)
            self.preferencesCoordinator?.start()
        } else {
            self.preferencesCoordinator?.focusWindow()
        }
    }
    
    func mainWindowClosed() {
        self.mainWindowCoordinator = nil
    }
    
    func preferencesWindowClosed(shouldSoftReset: Bool) {
        self.preferencesCoordinator = nil
        if shouldSoftReset {
            end()
            start()
        }
    }
    
    // MARK: Player management
    
    func initiatePlayback(tracks: [Track], startPlaybackFrom: Track?, collectionType: CollectionType) {
        let cursor = CollectionPlaybackCursor(tracks: tracks, startPlaybackFrom: startPlaybackFrom, collectionType: collectionType)
        cursor.repeatMode = self.repeatMode
        cursor.shuffleEnabled = self.shuffleEnabled
        
        if self.collectionPlayer != nil {
            stop()
        }
        
        // initialize player and start playing
        self.collectionPlayer = CollectionPlayer(playbackCursor: cursor)
        self.collectionPlayer?.play()
    }
    
    func playingCollectionMutated(tracks: [Track]) {
        self.collectionPlayer?.playbackCursor.updateTracks(newTracks: tracks)
    }
    
    func togglePlayback() {
        if (collectionPlayer?.isPlaying() ?? false) {
            pause()
        } else {
            play()
        }
    }
    
    func pause() {
        self.collectionPlayer?.pause()
    }
    
    func stop() {
        self.collectionPlayer?.pause()
        self.collectionPlayer = nil
    }
    
    func play() {
        self.collectionPlayer?.play()
    }
    
    func moveToPreviousTrack() {
        guard let track = collectionPlayer?.playbackCursor.currentTrack else { return; }
        self.libraryService?.increaseSkipCountForTrack(trackID: track.id)
        self.collectionPlayer?.previousTrack()
        self.mainWindowCoordinator?.refreshCurrentCollection()
    }
    
    func moveToNextTrack() {
        guard let track = collectionPlayer?.playbackCursor.currentTrack else { return; }
        self.libraryService?.increaseSkipCountForTrack(trackID: track.id)
        self.collectionPlayer?.nextTrack()
        self.mainWindowCoordinator?.refreshCurrentCollection()
    }
    
    func isPlayingTrack(_ track: Track) -> Bool {
        guard let player = self.collectionPlayer else {
            return false
        }
        guard let playingTrack = player.playbackCursor.currentTrack else {
            return false
        }
        return track.id == playingTrack.id
    }
    
    func isPlayingCollectionType(_ collectionType: CollectionType) -> Bool {
        guard let player = self.collectionPlayer else {
            return false
        }
        return player.playbackCursor.collectionType == collectionType
    }
    
    func playbackStatusForTrack(_ track: Track, inCollectionType collectionType: CollectionType) -> TrackPlaybackStatus {
        guard let player = self.collectionPlayer else { return .notPlaying }
        
        if !isPlayingCollectionType(collectionType) {
            return .notPlaying
        }
        
        guard let playingTrack = player.playbackCursor.currentTrack else {
            return .notPlaying
        }

        if track.contextualID(collectionType) == playingTrack.contextualID(collectionType) {
            if player.isPlaying() {
                return .playing
            } else {
                return .paused
            }
        } else {
            return .notPlaying
        }
    }
    
    func isPlaying(track: Track, inCollectionType collectionType: CollectionType) -> Bool {
        guard let player = self.collectionPlayer else {
            return false
        }
        if !isPlayingCollectionType(collectionType) {
            return false
        }
        guard let playingTrack = player.playbackCursor.currentTrack else {
            return false
        }
        return track.contextualID(collectionType) == playingTrack.contextualID(collectionType)
    }
    
    func importTrackToLibrary() {
        guard let service = self.libraryService else { return }
        
        // 1) give the user an open file dialog box to choose a track
        let openPanel = NSOpenPanel()
        openPanel.canChooseDirectories = false
        openPanel.canChooseFiles = true
        openPanel.allowsMultipleSelection = true
        openPanel.allowedFileTypes = AVURLAsset.audiovisualTypes().map() { $0.rawValue }

        let result = openPanel.runModal()
        if result == .OK {
            for fileUrl in openPanel.urls {
                // 2) extract the metadata
                let metadata = MetadataExtractor.extract(fileUrl)
                
                // 3) write track metadata to the library db
                service.addToLibraryCommand(trackFileURL: fileUrl, metadata: metadata)
            }

            // 4) reload the library table view
            self.mainWindowCoordinator?.refreshCurrentCollection()
        }

    }
    
    @objc func accountForTrackPlayed(notification: Notification) {
        let track = notification.object as! Track
        libraryService?.increasePlayCountForTrack(trackID: track.id)
        mainWindowCoordinator?.refreshCurrentCollection()
    }
}

enum TrackPlaybackStatus {
    case notPlaying
    case paused
    case playing
}
