// SPDX-License-Identifier: 0BSD

import Foundation
import AppKit
import UniformTypeIdentifiers

class PreferencesCoordinator {
    
    // MARK: Initializers
    required init(parentCoordinator: PreferencesSpawner) {
        self.parentCoordinator = parentCoordinator
    }
    
    // MARK: Properties
    // - parent coordinator
    var parentCoordinator : PreferencesSpawner
    // - window/view controllers
    var windowController : PreferencesWindowController?
    var pathChangeHandler : PathChangeHandler?
    // - state
    var originalPath : String?
    var modifiedPath : String? {
        didSet {
            self.pathChangeHandler?.libraryPathChanged(modifiedPath)
        }
    }
    
    // MARK: Flow management
    func start() {
        // load paths
        self.originalPath = MigrationHelper.currentLibraryPath()
        self.modifiedPath = self.originalPath
        // initialize window
        let storyboard = NSStoryboard.main
        let windowControllerID = NSStoryboard.SceneIdentifier("prefs-window-controller")
        self.windowController = storyboard?.instantiateController(withIdentifier: windowControllerID) as? PreferencesWindowController
        self.windowController?.coordinator = self
        self.windowController?.showWindow(self)
    }
    
    func end() {
        self.windowController?.close()
        let shouldSoftReset = self.originalPath != self.modifiedPath
        parentCoordinator.preferencesWindowClosed(shouldSoftReset: shouldSoftReset)
    }
    
    func focusWindow() {
        self.windowController?.window?.makeKeyAndOrderFront(self)
    }
    
    // MARK: User actions
    
    func resetToDefaults() {
        UserDefaults.standard.removeObject(forKey: "LibraryPath")
        self.modifiedPath = MigrationHelper.defaultLibraryPath()
    }
    
    func moveLibrary() {
        // save file dialog
        let savePanel = NSSavePanel()
        let utis = UTType.types(tag: "caelib", tagClass: UTTagClass.filenameExtension, conformingTo: nil)
        savePanel.allowedContentTypes = utis
        let result = savePanel.runModal()
        if result == .OK {
            guard let fileUrl = savePanel.url else { return }
            let oldPath = MigrationHelper.currentLibraryPath()
            let path = fileUrl.path

            // use path to NSFileManager move the current lib to the new path
            do {
                try FileManager.default.moveItem(atPath: oldPath!, toPath: path)
            } catch {
                return
            }
            
            // update UserDefaults path
            self.modifiedPath = path
            UserDefaults.standard.set(path, forKey: "LibraryPath")
        }
    }
    
    func loadDifferentLibrary() {
        // open file dialog
        let openPanel = NSOpenPanel()
        openPanel.canChooseDirectories = false
        openPanel.canChooseFiles = true
        openPanel.allowsMultipleSelection = false
        openPanel.allowedFileTypes = ["net.r-ch.caesura.library"]
        
        let result = openPanel.runModal()
        if result == .OK {
            guard let fileUrl = openPanel.url else { return }
            let path = fileUrl.path
            
            // update UserDefaults path
            self.modifiedPath = path
            UserDefaults.standard.set(path, forKey: "LibraryPath")
        }

    }
}
