// SPDX-License-Identifier: 0BSD

import Foundation
import AppKit

class PreferencesWindowController : NSWindowController {
    
    // MARK: Properties
    // - coordinator
    var coordinator : PreferencesCoordinator?

    // MARK: Window controller overrides
    override func windowDidLoad() {
        window?.title = "Preferences"
    }
    
    override func showWindow(_ sender: Any?) {
        // propagate coordinators to contained view controller
        guard let prefsViewController = self.window?.contentViewController as? PreferencesViewController else {
            return
        }
        prefsViewController.coordinator = self.coordinator
        self.coordinator?.pathChangeHandler = prefsViewController
        super.showWindow(sender)
    }

}
