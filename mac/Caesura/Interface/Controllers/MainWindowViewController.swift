// SPDX-License-Identifier: 0BSD

import Foundation
import AppKit

class MainWindowViewController : NSSplitViewController {
    
    var coordinator : MainWindowCoordinator?
    
    override func viewWillAppear() {
        
        guard let sourceListViewController = self.splitViewItems.first?.viewController as? SourceListViewController else {
            return
        }
        guard let collectionViewController = self.splitViewItems.last?.viewController as? CollectionViewController else {
            return
        }
        
        sourceListViewController.coordinator = self.coordinator
        collectionViewController.coordinator = self.coordinator
        self.coordinator?.sourceListReloadable = sourceListViewController
        self.coordinator?.collectionReloadable = collectionViewController

    }
}
