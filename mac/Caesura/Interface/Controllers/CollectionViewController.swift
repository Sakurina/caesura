// SPDX-License-Identifier: 0BSD

import Foundation
import Cocoa

class CollectionViewController : NSViewController, NSTableViewDelegate, NSTableViewDataSource, Reloadable {
    
    // MARK: Properties
    // - outlets
    @IBOutlet var table : NSTableView?
    // - coordinators
    var coordinator : MainWindowCoordinator?
    // - formatters
    var dateFormatter : DateFormatter?
    
    // MARK: View controller overrides
    
    override func viewDidAppear() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(reloadData),
                                               name: CaesuraNotification.trackChangedNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(reloadData),
                                               name: CaesuraNotification.playerStartedPlayingNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(reloadData),
                                               name: CaesuraNotification.playerStoppedPlayingNotification,
                                               object: nil)
        
        if (dateFormatter == nil) {
            dateFormatter = DateFormatter()
            dateFormatter?.dateFormat = "yyyy-MM-dd HH:mm:ss"
        }
        
        table?.registerForDraggedTypes([NSPasteboard.PasteboardType(rawValue: "net.r-ch.caesura.track-within-playlist")])
        table?.setDraggingSourceOperationMask(.move, forLocal: true)
        table?.doubleAction = #selector(tableViewDoubleClicked)
    }
    
    // MARK: Table view delegate
    
    func tableViewSelectionDidChange(_ notification: Notification) {
        // will be used to enable or disable the selection options in the menu bar
        guard let rowIndex = self.table?.selectedRow else { return }
        let isLibrary = (self.coordinator?.focusedSelectionIsLibrary() ?? true)
        // TODO: do this more elegantly with MenuStateHandler protocol on the coordinator side
        NSApp.castedAppDelegate().enableSelectionMenuItems(selected: rowIndex != -1, isLibrary: isLibrary)
    }
    
    func tableView(_ tableView: NSTableView, sortDescriptorsDidChange oldDescriptors: [NSSortDescriptor]) {
        guard let sortDescriptor = tableView.sortDescriptors.first else { return }
        let sortMode = sortDescriptor.key ?? "CollectionOrder"
        self.coordinator?.sortModeChanged(sortMode, ascending: sortDescriptor.ascending)
    }
    
    func tableView(_ tableView: NSTableView, shouldEdit tableColumn: NSTableColumn?, row: Int) -> Bool {
        return false
    }
    
    // MARK: Table view delegate (drag & drop)
    
    func tableView(_ tableView: NSTableView, pasteboardWriterForRow row: Int) -> NSPasteboardWriting? {
        return self.coordinator?.trackAtIndex(row)
    }
    
    func tableView(_ tableView: NSTableView, validateDrop info: NSDraggingInfo, proposedRow row: Int, proposedDropOperation dropOperation: NSTableView.DropOperation) -> NSDragOperation {
        
        switch self.coordinator?.collectionType {
            case .orderedPlaylist(_):
                // Needs to be in Order sort mode
                if !(self.coordinator?.currentlySortingByOrder() ?? true) {
                    return []
                }
            
                // Only allow drags between rows, not on top of rows
                if dropOperation == .above {
                    return .move
                } else {
                    return []
                }
            default: return []
        }
    }
    
    
    func tableView(_ tableView: NSTableView, acceptDrop info: NSDraggingInfo, row: Int, dropOperation: NSTableView.DropOperation) -> Bool {
        return self.coordinator?.handleReorderInCollectionDropOperation(rowIndex: row, info: info) ?? false
    }
    
    func tableView(_ tableView: NSTableView, draggingSession session: NSDraggingSession, endedAt screenPoint: NSPoint, operation: NSDragOperation) {
        self.coordinator?.refreshCurrentCollection()
    }
    
    
    // MARK: Table view data source
    
    func numberOfRows(in tableView: NSTableView) -> Int {
        return self.coordinator?.trackCount() ?? 0
    }
    
    func tableView(_ tableView: NSTableView, objectValueFor tableColumn: NSTableColumn?, row: Int) -> Any? {
        guard let t = self.coordinator?.trackAtIndex(row) else {
            return "(NO TRACK)"
        }
        
        // identifiers are properties of the table columns in the storyboard file
        if tableColumn?.identifier.rawValue == "CollectionOrder" {
            guard let collection = self.coordinator?.collectionType else { return "" }
            switch collection {
                case .orderedPlaylist(_): return t.order
                case .library: return ""
            }
        } else if tableColumn?.identifier.rawValue == "CollectionTitle" {
            return t.title
        } else if tableColumn?.identifier.rawValue == "CollectionArtist" {
            return t.artist
        } else if tableColumn?.identifier.rawValue == "CollectionAlbum" {
            return t.album
        } else if tableColumn?.identifier.rawValue == "CollectionPlayPause" {
            if let status = coordinator?.playbackStatusForTrackInFocusedCollection(t) {
                switch status {
                    case .playing: return "▶"
                    case .paused: return "❚❚"
                    case .notPlaying: return ""
                }
            }
            return ""
        } else if tableColumn?.identifier.rawValue == "CollectionPlayCount" {
            return t.play_count
        } else if tableColumn?.identifier.rawValue == "CollectionSkipCount" {
            return t.skip_count
        } else if tableColumn?.identifier.rawValue == "CollectionLastPlayed" {
            guard let last_played = t.last_played else { return "" }
            return dateFormatter?.string(from: last_played)
        }
        
        return "(UNKNOWN IDENTIFIER)"
    }
    
    // MARK: Double click handler
    
    @objc func tableViewDoubleClicked() {
        guard let clickedRowIndex = table?.clickedRow else { return }
        if clickedRowIndex < 0 {
            // Apparently the doubleAction gets called even when no row is selected
            // Specifically this happens, when clicking sort headers too quickly
            // This index checks prevents the app from crashing if you double-click sort headers
            return
        }
        guard let track = self.coordinator?.trackAtIndex(clickedRowIndex) else { return }
        coordinator?.initiatePlayback(startPlaybackFrom: track)
        self.table?.reloadData()
    }
    
    // MARK: Menu bar handlers
    
    @IBAction @objc func removeSelectedTracksFromFocusedCollection(_ sender: Any?) {
        guard let rowIndices = self.table?.selectedRowIndexes else { return }
        let tracks = rowIndices.map { rowIndex in
            return self.coordinator?.trackAtIndex(rowIndex)
        }
        for track in tracks {
            guard let track = track else { continue }
            self.coordinator?.removeTrackFromFocusedCollection(track)
        }
    }
    
    // MARK: Reloadable
    
    @objc func reloadData() {
        self.table?.reloadData()
    }
    
    func focus(_ index: Int) {
        return
    }
}
