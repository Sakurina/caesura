// SPDX-License-Identifier: 0BSD

import Foundation
import Cocoa

class SourceListViewController : NSViewController, NSOutlineViewDelegate, NSOutlineViewDataSource, Reloadable {
    
    // MARK: Properties
    // - outlets
    @IBOutlet var sourceList : NSOutlineView?
    
    // - coordinator
    var coordinator : MainWindowCoordinator?
    
    // MARK: View controller overrides
    
    override func viewDidAppear() {
        sourceList?.registerForDraggedTypes([NSPasteboard.PasteboardType(rawValue: "net.r-ch.caesura.track")])
        sourceList?.setDraggingSourceOperationMask(.copy, forLocal: true)
    }
        
    // MARK: Outline view data source
    
    func outlineView(_ outlineView: NSOutlineView, numberOfChildrenOfItem item: Any?) -> Int {
        if item == nil {
            return self.coordinator?.sourceListItemCount() ?? 0
        } else {
            return 0
        }
    }
    
    func outlineView(_ outlineView: NSOutlineView, objectValueFor tableColumn: NSTableColumn?, byItem item: Any?) -> Any? {
        let sourceListItem = item as! SourceListItem
        return sourceListItem.title
    }
    
    func outlineView(_ outlineView: NSOutlineView, isItemExpandable item: Any) -> Bool {
        return false
    }
    
    func outlineView(_ outlineView: NSOutlineView, child index: Int, ofItem item: Any?) -> Any {
        return self.coordinator!.sourceListItemAtIndex(index)
    }
    
    func outlineView(_ outlineView: NSOutlineView, shouldEdit tableColumn: NSTableColumn?, item: Any) -> Bool {
        let editTarget = item as! SourceListItem
        switch editTarget.collectionType {
            case .orderedPlaylist(_): return true
            default: return false
        }
    }
    
    // MARK: Outline view delegate
    
    func outlineViewSelectionDidChange(_ notification: Notification) {
        guard let rowIndex = self.sourceList?.selectedRow else { return }
        guard let selection = self.coordinator?.sourceListItemAtIndex(rowIndex) else { return }
        self.coordinator?.sourceChanged(selection)
        let isLibrary = selection.collectionType == .library
        // TODO: do this more elegantly with MenuStateHandler protocol on the coordinator side
        NSApp.castedAppDelegate().enableSelectionMenuItems(selected: rowIndex != -1, isLibrary: isLibrary)
    }
    
    func outlineView(_ outlineView: NSOutlineView, setObjectValue object: Any?, for tableColumn: NSTableColumn?, byItem item: Any?) {
        let newName = object as! String
        let renameTarget = item as! SourceListItem
        self.coordinator?.renameCollection(renameTarget.collectionType, toNewName: newName)
    }
    
    // MARK: Outline view delegate (Drag & drop)
    
    func outlineView(_ outlineView: NSOutlineView, validateDrop info: NSDraggingInfo, proposedItem item: Any?, proposedChildIndex index: Int) -> NSDragOperation {
        guard let dropDestination = item as? SourceListItem else { return [] }
        
        switch dropDestination.collectionType {
            case .orderedPlaylist(_): return .copy
            default: return []
        }
    }
    
    func outlineView(_ outlineView: NSOutlineView, acceptDrop info: NSDraggingInfo, item: Any?, childIndex index: Int) -> Bool {
        guard let dropDestination = item as? SourceListItem else { return false }
        return self.coordinator?.handleAddToCollectionDropOperation(dropDestination: dropDestination, info: info) ?? false
    }
    
    func outlineView(_ outlineView: NSOutlineView, draggingSession session: NSDraggingSession, endedAt screenPoint: NSPoint, operation: NSDragOperation) {
        self.coordinator?.refreshSourceListItems()
    }
    
    // MARK: Menu actions
    
    @IBAction @objc func createOrderedPlaylist(_ sender: Any?) {
        self.coordinator?.createOrderedPlaylist()
    }
    
    @IBAction @objc func deleteFocusedCollection(_ sender: Any?) {
        self.coordinator?.deleteFocusedCollection()
    }
    
    @IBAction @objc func renameFocusedCollection(_ sender: Any?) {
        guard let rowIndex = self.sourceList?.selectedRow else { return }
        self.sourceList?.editColumn(0, row: rowIndex, with: nil, select: true)
    }
    
    // MARK: Reloadable
    
    @objc func reloadData() {
        self.sourceList?.reloadData()
    }
    
    func focus(_ index: Int) {
        self.sourceList?.selectRowIndexes([index], byExtendingSelection: false)
    }
}
