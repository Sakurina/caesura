// SPDX-License-Identifier: 0BSD

import Foundation
import AppKit

class MainWindowController : NSWindowController {
    
    // MARK: Properties
    // - outlets
    @IBOutlet var playPauseToolbarItem : NSToolbarItem?
    
    // - coordinator
    var coordinator : MainWindowCoordinator?
    
    // MARK: Window controller overrides
    
    override func windowDidLoad() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(trackChanged),
                                               name: CaesuraNotification.trackChangedNotification,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(playerStartedPlaying),
                                               name: CaesuraNotification.playerStartedPlayingNotification,
                                               object: nil)

        NotificationCenter.default.addObserver(self,
                                               selector: #selector(playerStoppedPlaying),
                                               name: CaesuraNotification.playerStoppedPlayingNotification,
                                               object: nil)
    }
        
    override func showWindow(_ sender: Any?) {
        // propagate coordinators to contained view controller
        guard let mainWindowViewController = self.window?.contentViewController as? MainWindowViewController else {
            return
        }
        mainWindowViewController.coordinator = self.coordinator
        super.showWindow(sender)
    }
    
    // MARK: Interface actions
    
    @IBAction func play(_ sender: Any) {
        self.coordinator?.togglePlayback()
    }
    
    @IBAction func previous(_ sender: Any) {
        self.coordinator?.moveToPreviousTrack()
    }
    
    @IBAction func next(_ sender: Any) {
        self.coordinator?.moveToNextTrack()
    }
    
    @IBAction func searchTermsChanged(_ sender: NSSearchField) {
        self.coordinator?.keywordFilterTextChanged(sender.stringValue)
    }
    
    // MARK: Notification handlers
    
    @objc func trackChanged(_ notification : Notification) {
        guard let track = notification.object as! Track? else {
            return
        }
        self.window?.title = track.title ?? "Unknown Track"
        self.window?.subtitle = String(format: "%@ - %@", track.artist ?? "Unknown Artist", track.album ?? "Unknown Album")
    }
    
    @objc func playerStartedPlaying(_ notification : Notification) {
        playPauseToolbarItem?.image = NSImage(named: "NSTouchBarPauseTemplate")
    }
    
    @objc func playerStoppedPlaying(_ notification : Notification) {
        playPauseToolbarItem?.image = NSImage(named: "NSTouchBarPlayTemplate")
    }
}
