// SPDX-License-Identifier: 0BSD

import Foundation
import AppKit
import UniformTypeIdentifiers

class PreferencesViewController : NSViewController, PathChangeHandler {
    // MARK: Properties
    // - outlets
    @IBOutlet var libraryPathField : NSTextField?
    // - coordinator
    var coordinator : PreferencesCoordinator?
    
    override func viewWillAppear() {
        libraryPathField?.stringValue = self.coordinator?.originalPath ?? ""
    }
    
    override func viewWillDisappear() {
        self.coordinator?.end()
    }
    
    // MARK: Button actions
    
    @objc @IBAction func resetToDefaultsButtonPressed(sender: Any?) {
        self.coordinator?.resetToDefaults()
    }
    
    @objc @IBAction func moveLibraryButtonPressed(sender: Any?) {
        self.coordinator?.moveLibrary()
    }
    
    @objc @IBAction func loadDifferentLibraryButtonPressed(sender: Any?) {
        self.coordinator?.loadDifferentLibrary()
    }
    
    // MARK: Path change handler
    
    func libraryPathChanged(_ path: String?) {
        self.libraryPathField?.stringValue = path ?? ""
    }
}
