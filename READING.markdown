# Navigating the codebase

**Note:** This project is still in very early stages and thus the thought process around the organization of the codebase may change over time.

## Platforms

At the top level of this project, you can find the platform directories: **mac**, **ios**, and **common**.

**mac** and **ios** contain distinct apps for macOS and iOS/iPadOS respectively. We do not currently make use of Catalyst or SwiftUI to have a common UI layer across both apps, as neither of these technologies are mature enough to put together the experience I want on the Mac. (I currently have a better idea of what I want the Mac version to be, because it's trying to clone an old version of iTunes, so this version is currently given development priority.)

I hope to eventually extract code that can be shared across both platforms into a library under the **common** directory to facilitate feature parity.

## Within a Platform

TODO

